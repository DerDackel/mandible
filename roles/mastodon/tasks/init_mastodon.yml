---
- name: Clone mastodon repository
  git:
    repo: "https://github.com/tootsuite/mastodon.git"
    dest: "{{ mastodon.directory }}"
    version: "{{ mastodon.version | default('HEAD') }}"
  become: True
  become_user: mastodon

- name: Check if mastodon has already been configured
  stat: 
    path: "{{ mastodon.directory }}/.env.production"
  register: mastodon_configured 

- name: Generate secrets
  shell: "heat -c32 < /dev/urandom | sha512sum | cut -d ' ' -f 1"
  register: mastodon_raw_secrets
  with_items:
    - SECRET_KEY_BASE
    - PAPERCLIP_SECRET
    - OTP_SECRET

- name: Create default production env config
  copy:
    src: "{{ mastodon.directory }}/.env.production.sample"
    dest: "{{ mastodon.directory }}/.env.production"
    remote_src: True
  become: True
  become_user: mastodon
  when: not mastodon_configured.stat.exists

- name: Set Secrets
  lineinfile:
    path: "{{ mastodon.directory }}/.env.production"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    backrefs: False
  with_items:
    - { regexp: '^SECRET_KEY_BASE=.*$', line: "SECRET_KEY_BASE={{ mastodon_raw_secrets.results[0].stdout_lines[-1] }}" }
    - { regexp: '^OTP_SECRET=.*$', line: "OTP_SECRET={{ mastodon_raw_secrets.results[1].stdout_lines[-1] }}" }
    - { regexp: '^PAPERCLIP_SECRET=.*$', line: "PAPERCLIP_SECRET={{ mastodon_raw_secrets.results[2].stdout_lines[-1] }}" }
  become: True
  become_user: mastodon
  when: mastodon_configured.stat.exists == False

- name: Set configuration
  lineinfile:
    path: "{{ mastodon.directory }}/.env.production"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    backrefs: False
  with_items:
    - { regexp: '^SMTP_LOGIN=.*$', line: "SMTP_LOGIN={{ mastodon.smtp.login }}" }
    - { regexp: '^SMTP_PASSWORD=.*$', line: "SMTP_PASSWORD={{ mastodon.smtp.password }}" }
    - { regexp: '^SMTP_FROM_ADDRESS=.*$', line: "SMTP_FROM_ADDRESS={{ mastodon.smtp.from_address }}" }
    - { regexp: '^SMTP_SERVER=.*$', line: "SMTP_SERVER={{ mastodon.smtp.server }}" }
    - { regexp: '^SMTP_PORT=.*$', line: "SMTP_PORT={{ mastodon.smtp.port }}" }
    - { regexp: '^LOCAL_DOMAIN=.*$', line: "LOCAL_DOMAIN={{ mastodon.local_domain }}" }
  become: True
  become_user: mastodon

- stat:
    path: "{{ mastodon.directory }}/docker-compose-local.yml"
  register: docker_compose_copied

- name: Copy docker-compose.yml
  copy:
    src: "{{ mastodon.directory }}/docker-compose.yml"
    dest: "{{ mastodon.directory }}/docker-compose-local.yml"
    owner: mastodon
  when: docker_compose_copied.stat.exists == False
  become: True
  become_user: mastodon

# Hacky, needs improvement
- name: Enable Postgres docker volume
  replace:
    path: "{{ mastodon.directory }}/docker-compose-local.yml"
    regexp: '^#(\s+)volumes:\n#(\s+)- ./postgres:/var/lib/postgresql/data\n'
    replace: '    volumes:\n      - ./postgres:/var/lib/postgresql/data\n'
  become: True
  become_user: mastodon
  when: mastodon.docker.persistentdb == True

- name: Initial build of mastodon docker images
  shell: /usr/local/bin/docker-compose -f docker-compose-local.yml build
  args:
    chdir: "{{ mastodon.directory }}"
  become: True
  become_user: mastodon

- name: Run database migrations
  shell: /usr/local/bin/docker-compose -f docker-compose-local.yml run --rm web rails db:migrate
  args:
    chdir: "{{ mastodon.directory }}"
  become: True
  become_user: mastodon

- name: Prepare assets
  shell: /usr/local/bin/docker-compose -f docker-compose-local.yml run --rm web rails assets:precompile
  args:
    chdir: "{{ mastodon.directory }}"
  become: True
  become_user: mastodon

- name: Set permissions for image upload folder
  file:
    path: "{{ mastodon.directory }}/public/system"
    state: directory
    owner: "991"
    group: "991"
    mode: "0755"
  become: True

- name: Run Docker Compose deployment
  shell: /usr/local/bin/docker-compose -f docker-compose-local.yml up -d
  args:
    chdir: "{{ mastodon.directory }}"
  become: True
  become_user: mastodon
