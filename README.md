# Mandible - Mastodon + Ansible

Prototype Ansible playbook for installing Mastodon according to [this guide](https://github.com/ummjackson/mastodon-guide/blob/master/up-and-running.md).

Works on: Ubuntu 18.04, should also work on 16.04

**DISCLAIMER:** This repository is supposed to give you a quick way to play around with Mastodon on a VM or private server. Is is **NOT** intended for production use!

## How to run

 * Install the git, pip and ansible by running:

```
sudo apt-get install git python-pip
sudo pip install ansible
```

 * Clone this repo onto your new, freshly installed Ubuntu machine by running: 

```
git clone https://gitlab.com/DerDackel/mandible.git
cd mandible
```

 * Open up `playbook.yml` in your favorite editor e.g. 

```
nano playbook.yml
```

and edit the fields under the `vars` section in the file. Do not change the indentation of any lines, as the YAML file format relies on this being uniform!

```yaml
vars:
  mastodon:
    version: "v2.5.0rc1" # The release of Mastodon you want to deploy. 2.5.0rc1 is the current release at the point of writing.
    local_domain: "mastodon.example.com" # The URL your mastodon instance will be reached under
    letsencrypt_email: "admin@example.com" # An email address to use for id with letsencrypt
    docker:
        persistentdb: True # Have the instance's database persisted to your disk. If set to False, your instance's database contents will be deleted if you decide to delete the database container
    smtp: # All aspects of enabling your mastodon instance to send emails. You need an SMTP server available. If you do not have your own, you can register with an email service like [mailgun](https://www.mailgun.com).
        login: "someuser" # The login user for your SMTP server
        password: "somepassword" # The login user's password
        from_email: "awesomeelephant@" # The email address you want your users to see in the "From" field of the emails they'll receive from your instance. e.g. "notifications@mastodon.example.com"
        server: "smtp.mailgun.com" # The URL of the SMTP server you plan on using
        port: "587" # The port your SMTP server is listening under 
```

Once you're done, save your changes (e.g. `CTRL+O` in nano) then exit the editor (`CTRL+X` in nano).

 * Run

```
./setup.sh
```

You will be asked for your sudo password, as most of the actions required to set up mastodon also require root access.
 * Grab some coffee, stare at logs
 * Once everything is up, register on your newly created instance, confirm your account and then run

```
cd /opt/mastodon
sudo docker-compose -f docker-compose-local.yml run --rm web rails mastodon:make_admin USERNAME=yourusername
```

to make yourself admin of this instance.

## OK, what did this do?

* Created a service user called `mastodon`
* Cloned the official mastodon git repository to `/opt/mastodon` with the given relase version checked out
* Initiated the mastodon production environment config with the given mail settings and some randomly generated secret keys
* If `persistentdb` has been set to `True`: Make a subtle change in mastodon's original docker-compose.yml to enable database persistence. Data of the integrated PostgreSQL database will be saved to `/opt/mastodon/postgres` instead of inside the container.
* Built and deployed Mastodon Docker containers from the cloned repository
* Set the permissions of `/opt/mastodon/public/system` to the user and group used by Mastodon inside its Docker container, so Mastodon may save image uploads to disk under that location.
* Installed and configured nginx to act as a reverse proxy and terminate TLS in front of Mastodon
* Created a valid TLS certificate from Letsencrypt for said nginx to enable proper HTTPS connections.
